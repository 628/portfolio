import {createApp} from "vue";
import AOS from 'aos';
import './style.scss';
import {Chart} from "chart.js";

// @ts-ignore
import Swal from 'sweetalert2/dist/sweetalert2.js';

AOS.init();

const app = createApp({})

app.component('header-title', {
    props: ['txt'],
    template: '<div class="header-title"><h1>{{ txt }}</h1><hr/></div>'
})

app.component('skill-items', {
    props: ['skill', 'desc'],
    template: '<li v-bind:sandro-desc="desc"><a href="#">{{ skill }}</a></li>'
})

app.component('work-card', {
    props: ['title', 'desc', 'link', 'technologies'],
    template: '<div class="card">' +
        '<div class="icon-header">' +
        '<i class="las la-code-branch"></i>' +
        '<a :href="link" target="_blank"><i class="las la-external-link-alt"></i></a>' +
        '</div>' +
        '<h1>{{ title }}</h1>' +
        '<p>{{ desc }}</p>' +
        '<ul><li v-for="t in technologies">{{ t }}</li></ul>' +
        '</div>'
})

app.mount('#main')

window.onscroll = function () {
    let links = document.getElementById('links')

    if (links != null && links.style.opacity != "1" && window.scrollY > 250) {
        links.style.opacity = "1"
    }

    let opacity = (window.scrollY / -200.0) + 1;
    if (opacity > 1) opacity = 0;

    let arrDown = document.getElementById('arrowDown');

    if (arrDown == null) {
        return;
    }

    if (isNaN(parseInt(arrDown.style.opacity))) {
        arrDown.style.opacity = "1";
    }

    if (opacity < parseFloat(arrDown.style.opacity)) {
        arrDown.style.opacity = String(opacity);
    }
}

// @ts-ignore
document.getElementById('discord').onclick = function (e) {
    e.preventDefault();

    Swal.fire({
        title: "Tau#2774"
    })
}

window.addEventListener('load', (_) => {
    // @ts-ignore
    document.getElementById('loading').style.display = 'none';
    document.getElementsByTagName('body')[0].style.overflow = 'auto';
})
